<?php
 error_reporting(E_ERROR);
 ini_set('display_errors', 1);
 header("content-type: text/html; charset=utf-8");
 $page = @$_REQUEST['page'];
 $section = @$_REQUEST['section'];
 $note = @$_REQUEST['note'];
 include "./web_config/globals.php";
 include ".". Globals::$GLOBAL_SQL_FILE;
 include "./lib/lib.php";
 include "./lib/pageswitcher.php";
 include "./lib/pager.php";
 include "./lib/seo.php";
 $time = time();
 $subnote = explode ('-',$page);
 echo "<!-- finp-".$finalpage."|page-".$page."|section-".$section."|finalID-".$finalID."|finalID-".$finalID.$whatyouwant." -->";
?><!DOCTYPE html>

<html lang="cs">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <!--<meta name="viewport" content="width=device-width; initial-scale=1, minimum-scale=1, maximum-scale=1">-->
  <meta name="viewport" content="width=device-width, initial-scale=1.0,  minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
  <title><?php echo pageWTF(@$finalpage,@$page,@$finalsection,@$finalID,@$finalnewsid,'title'); ?></title>
  <meta name='description' content='<?php echo pageWTF(@$finalpage,@$page,@$finalsection,@$finalID,@$finalnewsid,'desc'); ?>' />
  <meta name='keywords' content='<?php echo pageWTF(@$finalpage,@$page,@$finalsection,@$finalID,@$finalnewsid,'keywords'); ?>' /> 
  <!-- Facebook --> 
  <?php 
   if(strlen(pageWTF(@$finalpage,@$page,@$finalsection,@$finalID,@$finalnewsid,'fbimg'))>0){
  ?><meta property="og:image" content="<?php echo pageWTF(@$finalpage,@$page,@$finalsection,@$finalID,@$finalnewsid,'fbimg'); ?>" />    
  <?php
  }
  ?><meta property="og:title" content="<?php echo pageWTF(@$finalpage,@$page,@$finalsection,@$finalID,@$finalnewsid, 'title'); ?>"/>
  <meta property="og:url" content="<?php  echo 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; ?>"/>
  <meta property='og:description' content='<?php echo pageWTF(@$finalpage,@$page,@$finalsection,@$finalID,@$finalnewsid, 'desc'); ?>' />   
  <meta property="og:type" content="website" />     
  <!-- End Facebook -->   
  <link rel="apple-touch-icon" sizes="180x180" href="/favicons/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="/favicons/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="/favicons/favicon-16x16.png">
  <link rel="manifest" href="/favicons/site.webmanifest">
  <link rel="mask-icon" href="/favicons/safari-pinned-tab.svg" color="#fff">
  <link rel="shortcut icon" href="/favicons/favicon.ico">
  <meta name="msapplication-TileColor" content="#fff">
  <meta name="theme-color" content="#fff">
  <meta name="robots" content="index,follow" />
  <?php if(Globals::$GLOBAL_HEADERS_JQUERY == 1) { ?><script src="//code.jquery.com/jquery-latest.js"></script>
  <?php } ?>
<script  src="<?php echo $tempdir; ?>/js/scripts.js"></script>
  <?php if(Globals::$GLOBAL_HEADERS_STICKY == 1) { ?><script src="<?php echo $tempdir; ?>/js/jquery.sticky.js"></script>
  <?php } ?>
<?php if(Globals::$GLOBAL_HEADERS_FONTAWESOME == 1) { ?><link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
  <?php } ?>
  <?php if(Globals::$GLOBAL_HEADERS_SLICK == 1) { ?><link rel="stylesheet" type="text/css" href="<?php echo $tempdir; ?>/slick/slick.css">
  <link rel="stylesheet" type="text/css" href="<?php echo $tempdir; ?>/slick/slick-theme.css">
  <script src="<?php echo $tempdir; ?>/slick/slick.js" type="text/javascript" charset="utf-8"></script>
  <?php } ?>  

 
  <?php if(Globals::$GLOBAL_HEADERS_sWIPEBOX == 1) { ?><script src="<?php echo $tempdir; ?>/swipebox-master/src/js/jquery.swipebox.js"></script>
  <link rel="stylesheet" href="<?php echo $tempdir; ?>/swipebox-master/src/css/swipebox.css">
  <?php } ?>  
  <!--  
  <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap&subset=latin-ext" rel="stylesheet">
  --> 
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@600&family=Roboto:wght@300;400;500;700&display=swap&subset=latin-ext" rel="stylesheet">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
  <link href="<?php echo $tempdir; ?>/css/main.css?ver=0.<?php //echo time(); ?>" rel="stylesheet" type="text/css" />    
  <?php if(Globals::$GLOBAL_HEADERS_TINYCSS == 1) { ?><link rel="stylesheet" type="text/css" href="<?php echo $tempdir; ?>/css/tiny.css"><?php } ?>
  <?php if(Globals::$GLOBAL_HEADERS_RESPCSS == 1) { ?><link href="<?php echo $tempdir; ?>/css/response.css?ver=0.<?php //echo time(); ?>" rel="stylesheet" type="text/css" /></script>
  <?php } ?>    
  <link href="t" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="<?php echo $tempdir; ?>/css/burger.css">
</head>
<?php
  if(strlen($_GET['bodyClass'])>0) { $bodyclass = $bodyclass."  ".$_GET['bodyClass']; }
  if(strlen($_GET['superBodyClass'])>0) { $superBodyClass = $superBodyClass."  ".$_GET['superBodyClass']; }  
  if( $ishome == 1) { $homeClass = 'home'; }  

?>
<body  class="<?php echo $bodyclass; ?> <?php echo $superBodyClass; ?> <?php echo $homeClass; ?> ">
<header>
  <div id='tophead'>
      <a href='' class='soc fb'>FB</a>
      <a href='' class='soc ln'>LN</a>
      <a class='tel' href='tel:+420606024647'>+420 606 024 647</a>
      <a class='ema' href='mailto:amb@recoveryamb.cz'>amb@recoveryamb.cz</a>
      <span class='ord'>objednat se můžete ihned</span>
  </div>
  <nav>
   <div id='webtitle'>recoverλ centrum</div>
   <ul class="menu" id="mainMenu">
   <?php
   $type = 0;
   $type2 = 10000;   
   include ('./inside/menu.php'); 
   ?> 
   </ul>
   <div class="menu-toggle"><div class="hamburger"></div></div>
   <ul class='desktop'><?php  include ("./inside/menu.php"); ?></ul>
   </nav> 
  
</header>
<?php include ('./inside/banners.php');   ?>
<main id="main">
 
    <?php 
 
     if(strlen($finalpage) > 1) {
     $isOneQuery = "Select menuOnepage From tblpages,menugroups Where menugroups.ID = tblpages.pagesMenu And pagesLink = '".$page."'";
     }
     else{
       $isOneQuery = "Select menuOnepage From tblpages,menugroups Where menugroups.ID = tblpages.pagesMenu And pagesHomepage = 1";  
     }
     //echo $isOneQuery;
     $isOneRes = $link->query($isOneQuery);
     if ($isOneRes && mysqli_num_rows($isOneRes)>0)
     {
        while ($isOneRow = mysqli_fetch_array($isOneRes)){
          $isOnepage = $isOneRow['menuOnepage'];
        }
     }        
    
    //pokud podle menu patri aktualni finalpage do Onepage... 
    if($isOnepage == 1) { 
     $onepagequery2 = "SELECT * FROM `tblpages` WHERE   `pagesMenu` = ".$findedID ." ANd pagesMother = 0 ORDER BY `pagesOrder` ASC  ";
     $onepagexres2 = $link->query($onepagequery2);
    if ($onepagexres2 && mysqli_num_rows($onepagexres2)>0)
    {
       while ($onepagerow2 = @mysqli_fetch_array($onepagexres2)){
       $onepages[] = $onepagerow2['pagesLink'];  
       $onepageID[] = $onepagerow2['ID'];  


    }
    } 
    $pagecount = 0;  
    foreach($onepages as $onepage)
	  {  
       $nextpage[] = $onepage;
    }   
    foreach($onepages as $onepage)          
	  {  

    $finalpage = $onepage;
       
       /* hledam bgr obr */

        $imgSearch = "SELECT ppFoto FROM tblpageparts,tbl_hmimg WHERE tbl_hmimg.Id = tblpageparts.ppID And ppPerc = 'pgbgr' And `ppPage` = ".$onepageID[$pagecount]." AND `ppType` LIKE 'tbl_hmimg' AND `ppDelete` != 1";
        $imgRes = $link->query($imgSearch);
       if ($imgRes && mysqli_num_rows($imgRes)>0)
       {
          while ($imgRow = @mysqli_fetch_array($imgRes)){
           $bgr = ' style="background-image: url(';
           $bgr .= "'/Uploads/web_images/";
           $bgr .= $imgRow['ppFoto'];
           $bgr .= "') ";
           $bgr .= ' "'  ; 
          }
       }
       else{
          $bgr = '';
       }
    
	  ?>
    <section id="<?php echo $onepage; ?>" class="sliders" <?php echo $bgr; ?>> 
	  <article >
     
	  <?php
    include ('./inside/content.php');  
    ?>
    <div class="mainclr"></div> 
    
    </article>
    <?php 
    //nacitam stranky do boku

    $onepagequery3 = "SELECT * FROM `tblpages` WHERE   `pagesMenu` = ".$findedID ." ANd pagesMother = ".$onepageID[$pagecount]." ORDER BY `pagesOrder` ASC  ";
    $onepagexres3 = $link->query($onepagequery3);
    if ($onepagexres3 && mysqli_num_rows($onepagexres3)>0)
    {
       while ($onepagerow3 = @mysqli_fetch_array($onepagexres3)){
       ?>
      <article >
            <div class="ingroup"> 
            <?php
               $finalpage = $onepagerow3['pagesLink'];
               include ('./inside/content.php');  
            ?>
             <div class="mainclr"></div> 
             </div>
      </article>
       <?php
       }
    }
    ?>

<?php
            if($pagecount>-1) {
            ?>
            <div class="mainclr  "></div>
            <?php
            if(strlen($nextpage[$pagecount+1])>0) {
            $SElNxtName = "Select pagesTitle From tblpages Where pagesLink = '".$nextpage[$pagecount+1]."' ";
            $nxtNameres = $link->query($SElNxtName);
            if ($nxtNameres && mysqli_num_rows($pageres)>0)
            {  
            while ($nxtNamerow = @mysqli_fetch_array($nxtNameres)){
                   $nxtName = $nxtNamerow['pagesTitle'];
            }
            }
            ?>
            <p class="aftersec"><a class='nxt inpage' href="#<?php echo $nextpage[$pagecount+1]; ?>" title="Go to <?php echo  $nxtName; ?>">NEXT</a></p> 
            <?php
              }
              }
            ?>

</section>



    <?php
   if ($onepagexres3 && mysqli_num_rows($onepagexres3)>0)
   {
     ?>
<script type="text/javascript">
$(document).ready(function() { 
  $('#<?php echo $onepage; ?>').slick({
  dots: true,
  infinite: true,
  speed:1000,
  slidesToShow: 1,
  adaptiveHeight: true,
  autoplay: false
  //fade: true,
  //cssEase: 'linear'
});
});
</script>
 
    <?php
   }
    $pagecount++;
    }
  }
  //konec Onepage


  else{
 
    include ('./inside/contentNoSingle.php');  
     
  }


    ?>

    <div class="mainclr"></div> 
</main>
 
<footer>
  <p>recovery Terapeutické centrum, Žitná 52, Praha 2, 120 00, +420 606 024 647, www.recoveryamb.cz<br/>
provozovatel: SANANIM SOS, s.r.o., IČO 24244368, spisová zn. C 196727/MSPH, sídlo: Na Skalce 819/15, 150 00 Praha 5. </p>
</footer>

<a href="nav" class="cd-top" > </a>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-BWL01WW7K2"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-BWL01WW7K2');
</script>


</body>
</html>
 