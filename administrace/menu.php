 
<?php
  $menui = 0;
  $x = 0;
  $active = '';
  foreach($_GET as $key => $value){
    if($key == 'id'){
      $active .= $value;
    }
    else{
      $active .= "&".$key."=".$value;
    }
  }
  foreach($_GET as $key => $value){
    if($key == 'id'){
       $active2 .= str_replace ('new','',$value);
    }
    else{
      //$active2 .= "&".$key."=".$value;
    }
  }
 
$priviledge_query = "Select * from tblpriviledge Where Deleted = 0 order by Description ";
  $priviledge_res = @$link->query($priviledge_query);
  if ($priviledge_res && @mysqli_num_rows($priviledge_res)>0)
  {
	  while ($rowExist = @mysqli_fetch_array($priviledge_res))
	  {
      $menuArr[] = $rowExist[InLib];
    }
  }  
   
  
  if (in_array("PRIVILEDGE_CONTENT", $menuArr)){
  $priviledge[$x] =  PRIVILEDGE_CONTENT ;
  $MymenuName[$x] = 'Dashboard';
  $menuItemUrl[$x]  = 'dashboard';  
  $ico[$x] = 'fas fa-chart-line';
  $solo[$x] = 1;
  $x++;
  }

  if (in_array("PRIVILEDGE_CONTENT", $menuArr)){
  $priviledge[$x] =  PRIVILEDGE_CONTENT ;
  $MymenuName[$x] = 'Stránky';
  $menuItemUrl[$x]  = 'pages';  
  $ico[$x] = 'fas fa-home';
  $solo[$x] = 1;
  $x++;
  }
  if (in_array("PRIVILEDGE_SECTIONS", $menuArr)){
    $priviledge[$x] =  PRIVILEDGE_SECTIONS ;
    $MymenuName[$x] = 'Sekce (Menu)';
    $menuItemUrl[$x]  = 'menugroups';  
    $ico[$x] = 'fas fa-bars';
    $solo[$x] = 1;
    $x++;
    }
     
  if (in_array("PRIVILEDGE_AKTUAL", $menuArr)){
  $priviledge[$x] =  PRIVILEDGE_AKTUAL ;
  $MymenuName[$x] = 'Aktuality';
  $menuItemUrl[$x]  = 'pages&type=aktualne';  
  $solo[$x] = 1;
  $ico[$x] = 'far fa-comment';
  $x++;
  }

  if (in_array("PRIVILEDGE_ARTICLES", $menuArr)){
    $priviledge[$x] =  PRIVILEDGE_ARTICLES ;
    $MymenuName[$x] =   Globals::$GLOBAL_MODULES_BLOG ;
    $menuItemUrl[$x]  = 'pages&type=articles';  
    $solo[$x] = 1;
    $ico[$x] = 'far fa-newspaper';
    $x++;
    }


if (in_array("PRIVILEDGE_CATS", $menuArr)){
  $priviledge[$x] =  PRIVILEDGE_CATS ;
  $MymenuName[$x] = 'Správa kategorií';
  $menuItemTxt[$x][0] = 'Kategorie blogu' ;
  $menuItemUrl[$x][0] = 'kat&catname=artcats';
  $menuItemTxt[$x][1] = 'Autoři blogů' ;
  $menuItemUrl[$x][1] = 'kat&catname=blogers';
  $submenuCount[$x] = 2;
  $ico[$x] = 'fas fa-layer-group';
  $solo[$x] = 0;
  $x++;
}

  if (in_array("PRIVILEDGE_PHOTO", $menuArr)){
  $priviledge[$x] =  PRIVILEDGE_PHOTO ;
  $MymenuName[$x] = 'Fotogalerie';
  $menuItemUrl[$x]  = 'galery';  
  $solo[$x] = 1;
  $ico[$x] = 'far fa-image';
  $x++;
  }

  if (in_array("PRIVILEDGE_DOWNLOADS", $menuArr)){
  $priviledge[$x] =  PRIVILEDGE_DOWNLOADS ;
  $MymenuName[$x] = 'Downloads  ';
  $menuItemUrl[$x]  = 'downloads';  
  $solo[$x] = 1;
  $ico[$x] = 'fas fa-cloud-download-alt';
  $x++;
  }


  
  if (in_array("PRIVILEDGE_CONTENT_LANG", $menuArr)){
  $priviledge[$x] =  PRIVILEDGE_CONTENT_LANG ;
  $MymenuName[$x] = 'Jazykové verze';
  $menuItemTxt[$x][0] = 'Obsah EN' ;
  $menuItemTxt[$x][1] = 'Obsah DE';  
  $menuItemTxt[$x][2] = 'Obsah PL';  
  $menuItemTxt[$x][3] = 'Sekce webu EN';
  $menuItemTxt[$x][4] = 'Sekce webu DE';
  $menuItemTxt[$x][5] = 'Sekce webu PL';

  $menuItemUrl[$x][0] = 'pages&type=en';
  $menuItemUrl[$x][1] = 'pages&type=de';
  $menuItemUrl[$x][2] = 'pages&type=pl';
  $menuItemUrl[$x][3] = 'menugroups&lang=en';
  $menuItemUrl[$x][4] = 'menugroups&lang=de';
  $menuItemUrl[$x][5] = 'menugroups&lang=pl';
  $ico[$x] = 'fas fa-globe';
  $submenuCount[$x] = 6;
  $x++;
  }
 
  if (in_array("PRIVILEDGE_HEROS", $menuArr)){
    $priviledge[$x] =  PRIVILEDGE_HEROS ;
    $MymenuName[$x] = 'Bannery';
    $menuItemUrl[$x]  = 'banners';  
    $menuItemUrlEN[$x] = 'banners&lang=en';
    $ico[$x] = 'fas fa-bullhorn';

    $solo[$x] = 1;
    $x++;
  }
  if (in_array("PRIVILEDGE_TEAM", $menuArr)){ 
    $priviledge[$x] =  PRIVILEDGE_TEAM ;
    $MymenuName[$x] = 'Tým  ';
    $menuItemUrl[$x]  = 'members';  
    $solo[$x] = 1; 
    $ico[$x] = 'fas fa-users';
    $x++;
   }

if (in_array("xxxPRIVILEDGE_ADDS", $menuArr)){
  $priviledge[$x] =  PRIVILEDGE_ADDS ;
  $MymenuName[$x] = 'Bannery';
  $menuItemTxt[$x][0] = 'CZ banners' ;
  $menuItemUrl[$x][0] = 'banners';  
  $menuItemTxt[$x][1] = 'EN banners';
  $menuItemUrl[$x][1] = 'banners&lang=en';
  $menuItemTxt[$x][2] = 'DE banners';
  $menuItemUrl[$x][2] = 'banners&lang=de';  
  $menuItemTxt[$x][3] = 'PL banners';
  $menuItemUrl[$x][3] = 'banners&lang=pl';  
  $submenuCount[$x] = 4;
  $ico[$x] = 'fas fa-bullhorn';
  $x++;
}

if (in_array("PRIVILEDGE_HMBOX", $menuArr)){
  $priviledge[$x] =  PRIVILEDGE_HMBOX ;
  $MymenuName[$x] = 'Widgety';
  $menuItemUrl[$x]  = 'pages&type=hmboxes';  
  $ico[$x] = 'far fa-clipboard';
  $solo[$x] = 1;
  $x++;
}

if (in_array("PRIVILEDGE_REGZ", $menuArr)){
  $priviledge[$x] =  PRIVILEDGE_REGZ ;
  $MymenuName[$x] = 'Rezervace';
  $menuItemUrl[$x]  = 'letter';  
  $solo[$x] = 1; 
  $ico[$x] = 'far fa-envelope';
  $x++;
}

if (in_array("PRIVILEDGE_MICRODATA", $menuArr)){
  $priviledge[$x] =  PRIVILEDGE_MICRODATA ;
  $MymenuName[$x] = 'Kontakt - microdata';
  $menuItemUrl[$x]  = 'contacts';  
  $solo[$x] = 1;
  $ico[$x] = 'far fa-envelope';
  $x++;
}
/*
if (in_array("PRIVILEDGE_CATS", $menuArr)){
  $priviledge[$x] =  PRIVILEDGE_CATS ;
  $MymenuName[$x] = 'Správa kategorií';
  $menuItemTxt[$x][0] = 'Kategorie galerií' ;
  $menuItemUrl[$x][0] = 'kat&catname=galcats';  
  $menuItemTxt[$x][1] = 'Kategorie aktualit';
  $menuItemUrl[$x][1] = 'kat&catname=aktucats';
  $menuItemTxt[$x][2] = 'Kategorie downloads';
  $menuItemUrl[$x][2] = 'kat&catname=downloads';
  $menuItemTxt[$x][3] = 'Kategorie Zařízení';
  $menuItemUrl[$x][3] = 'kat&catname=artcats';
  $submenuCount[$x] = 4;
  $ico[$x] = 'fas fa-layer-group';
  $solo[$x] = 0;
  $x++;
}


*/
if (in_array("PRIVILEDGE_FAQ", $menuArr)){
  $priviledge[$x] =  PRIVILEDGE_FAQ ;
  $MymenuName[$x] = 'F.A.Q. ';
  $menuItemUrl[$x]  = 'faq';  
  $solo[$x] = 1; 
  $ico[$x] = 'far fa-question-circle';
  $x++;
}



 if (in_array("PRIVILEDGE_POOLS", $menuArr)){ 
  $priviledge[$x] =  PRIVILEDGE_POOLS ;
  $MymenuName[$x] = 'Dotazníky';
  $menuItemUrl[$x]  = 'quests2';  
  $solo[$x] = 1; 
  $ico[$x] = 'far fa-question-circle';
 
  $x++;
 }

if (in_array("PRIVILEDGE_MEMBERS", $menuArr)){
  $priviledge[$x] =  PRIVILEDGE_MEMBERS ;
  $MymenuName[$x] =   Globals::$GLOBAL_MODULES_MEMBERS ; 
  $menuItemUrl[$x]  = 'members';  
  $solo[$x] = 1; 
  $ico[$x] = 'fas fa-user-tag';
  $x++;
}
if (in_array("PRIVILEDGE_FILEMANAGER", $menuArr)){
  $priviledge[$x] =  PRIVILEDGE_FILEMANAGER ;
  $MymenuName[$x] = 'Správa souborů';
  $menuItemUrl[$x]  = 'filemanager';  
  $solo[$x] = 1; 
  $ico[$x] = 'fas fa-sitemap';
  $x++;
}



  $priviledge[$x] =  PRIVILEDGE_USERS ;
  $MymenuName[$x] = 'Správa uživatelů';
  $menuItemUrl[$x]  = 'users';  
  $solo[$x] = 1; 
  $ico[$x] = 'fas fa-users';
  $x++;

  $priviledge[$x] =  PRIVILEDGE_USERS ;
  $MymenuName[$x] = 'Gen. Settings';
  $menuItemUrl[$x]  = 'service';  
  $solo[$x] = 1; 
  $ico[$x] = 'fas fa-cogs';
  $x++;

 

 

?>
 
<?php
$liac == '';
for ($menui = 0; $menui <  count($priviledge); $menui++) {
if( has_priviledge( $priviledge[$menui] ) ) 
{
  
  
  
if($solo[$menui] == 1) {

if(($active2 == $menuItemUrl[$menui]) || ($active == $menuItemUrl[$menui])) {   $liac = ' class="active"'; } else {  $liac = ''; }
 
?>
<ul class="list-unstyled components">
   <li <?php echo $liac; ?>>
   <a class='<?php echo $ico[$menui]; ?>' href="index.php?id=<?php echo $menuItemUrl[$menui]; ?>"><span><?php echo $MymenuName[$menui]; ?></span></a>
   </li>
</ul>              
<?php
}
else{
?>
<ul class="list-unstyled components">
            <li>
                <a href="#submeu<?php echo $menui; ?>" data-toggle="collapse" aria-expanded="true" class="dropdown-toggle  <?php echo $ico[$menui]; ?>">
                <span><?php echo $MymenuName[$menui]; ?></span></a>
                <?php
                for ($menuItem = 0; $menuItem <  $submenuCount[$menui]; $menuItem++) {
                if($active == $menuItemUrl[$menui][$menuItem]) { $ulac = ' show '; }  
                  }
                 ?>
 
                <ul class="collapse list-unstyled <?php echo $ulac; ?>" id="submeu<?php echo $menui; ?>">
                    <?php 
                      for ($menuItem = 0; $menuItem <  $submenuCount[$menui]; $menuItem++) {
                      if($active == $menuItemUrl[$menui][$menuItem]) { $liac = ' class="active"'; } else { $liac = ''; }
                    ?>
                    <li <?php echo $liac; ?>>
                        <a href="index.php?id=<?php echo $menuItemUrl[$menui][$menuItem]; ?>"><?php echo $menuItemTxt[$menui][$menuItem]; ?></a>
                    </li>
                    <?php
                    }
                    ?>
                </ul>
            
              </li>


              

        </ul>
  <?php
  $liac == '';
  $ulac = '';
  }
  }

  }
 

 
?>
 <ul class="list-unstyled components">
   <li>
   <a class="fas fa-globe-africa" href="/eng/administrace"><span>ENG</span></a>
   </li>
</ul>
