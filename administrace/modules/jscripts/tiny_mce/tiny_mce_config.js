   tinyMCE.init({
  selector: 'textarea',
 /* plugins: "anchor",
  toolbar: "anchor",
  menubar: "insert"*/
 /* width: 1000,*/
  theme: 'modern',
  convert_urls : 0, // default 1
  menu : { // this is the complete default configuration  
   edit   : {title : 'Edit'  , items : 'undo redo | cut copy paste pastetext | selectall'},
   insert : {title : 'Insert', items : 'link media  hr anchor'},
   format : {title : 'Font format', items : 'bold italic underline strikethrough superscript subscript   '},
   table  : {title : 'Table' , items : 'inserttable deletetable | cell row column '},  
 },    
 plugins: [
   'advlist autolink lists link image charmap   preview hr anchor pagebreak  ',
   'searchreplace wordcount visualblocks visualchars code fullscreen',
   'insertdatetime media nonbreaking save table contextmenu directionality',
   'emoticons  paste textcolor colorpicker textpattern imagetools codesample toc help'
 ],
 style_formats : [
   { title: 'Nadpisy', items: [
     { title: 'h2', block: 'h2' },
     { title: 'h3', block: 'h3' },
     { title: 'h4', block: 'h4' },
     { title: 'h5', block: 'h5' },
     { title: 'h6', block: 'h6' }
   ] },

   { title: 'Bloky', items: [
     { title: 'p', block: 'p' },
     { title: 'div', block: 'div' },
     { title: 'pre', block: 'pre' }
   ] } 
   ,

    { title: 'Speciální', items: [
      {title:"Ico Telefon", selector:"p", classes:"fas fa-phone-square"},
      {title:"Ico Obalka", selector:"p", classes:"far fa-envelope"}

 
 
    ] }  
  
   ],
 toolbar1: 'undo redo |  | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | code   ',
 toolbar2: 'styleselect  | link image media | preview  | forecolor backcolor  ',
 image_advtab: true,
   insertdatetime_formats: ["%H:%M", "%d. %m. %Y"],
   paste_as_text: true,
 
 /*  external_filemanager_path:"https://recoveryamb.cz/administrace/Filemanager/filemanager/",
   filemanager_title:"Responsive Filemanager" ,
   external_plugins: { "filemanager" : "https://recoveryamb.cz/administrace/Filemanager/filemanager/plugin.min.js"},*/
 
  content_css: [
    /*'//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
    '//www.tinymce.com/css/codepen.min.css',
    '/css/tiny.css?anticache=' + new Date().getTime(),*/
    '/css/tiny.css?anticache=' + new Date().getTime()
  ]
		 
	});

function kickstart_Tiny(){
 
	tinyMCE.init({
  selector: 'textarea',
 
  width: 700,
  theme: 'modern',
  convert_urls : 0, // default 1
   menu : { // this is the complete default configuration  
    edit   : {title : 'Edit'  , items : 'undo redo | cut copy paste pastetext | selectall'},
    insert : {title : 'Insert', items : 'link media  hr anchor'},
    format : {title : 'Font format', items : 'bold italic underline strikethrough superscript subscript   '},
    table  : {title : 'Table' , items : 'inserttable deletetable | cell row column '},  
  },    
  plugins: [
    'advlist autolink lists link image charmap   preview hr anchor pagebreak',
    'searchreplace wordcount visualblocks visualchars code fullscreen',
    'insertdatetime media nonbreaking save table contextmenu directionality',
    'emoticons  paste textcolor colorpicker textpattern imagetools codesample toc help'
  ],
  style_formats : [
    { title: 'Nadpisy', items: [
      { title: 'h2', block: 'h2' },
      { title: 'h3', block: 'h3' },
      { title: 'h4', block: 'h4' },
      { title: 'h5', block: 'h5' },
      { title: 'h6', block: 'h6' }
    ] },

    { title: 'Bloky', items: [
      { title: 'p', block: 'p' },
      { title: 'div', block: 'div' },
      { title: 'pre', block: 'pre' }
    ] } ,

    { title: 'Speciální', items: [
      {title:"Obrázek vlevo", selector:"img", classes:"imgleft"},
      {title:"Obrázek vpravo", selector:"img", classes:"imgright"},
    ] } 
    ],
  toolbar1: 'undo redo |  | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | code   ',
  toolbar2: 'styleselect    | link image media | preview  | forecolor backcolor  ',
  image_advtab: true,
   insertdatetime_formats: ["%H:%M", "%d. %m. %Y"],
   paste_as_text: true,

 
 /*  external_filemanager_path:"https://recoveryamb.cz/Filemanager/filemanager/",
   filemanager_title:"Responsive Filemanager" ,
   external_plugins: { "filemanager" : "https://recoveryamb.cz/Filemanager/filemanager/plugin.min.js"},
 */
  content_css: [
    /*'//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
    '//www.tinymce.com/css/codepen.min.css',
    '/css/tiny.css?anticache=' + new Date().getTime(),*/
    '/css/tiny.css?anticache=' + new Date().getTime()
  ]
	});
}
 