<script type="text/javascript" language="javascript">kickstart_Tiny();</script>
<style>
.input-group-prepend, .input-group-text {
   min-width: 160px; 
}
</style>
<?php 
$table_name = "tbluser";
$page_name = "users";
$priviledge_arr = false;

if (isset($_REQUEST["newsid"]) && is_numeric($_REQUEST["newsid"]) && $_REQUEST["newsid"]>0)
{
	$user_id = $_REQUEST["newsid"];
}
else
{
	$user_id = false;
	$row["UCanLogin"] = 1;
}

if ($user_id)
{
	$query = "Select * from ".$table_name." where ID = ".$user_id." and Deleted = 0";
	$res = @$link->query($query);

	if ($res && @mysqli_num_rows($res)>0)
	{
		$row = @mysqli_fetch_array($res);
	}

	$query = "Select PUPriviledge from tblpriviledgeuser where PUUser = ".$user_id." and Deleted = 0";
	$res = @$link->query($query);

	if ($res && @mysqli_num_rows($res)>0)
	{
		$priviledge_arr = array();
		while ($priv_row = @mysqli_fetch_array($res))
		{
			$priviledge_arr[] = $priv_row["PUPriviledge"];
		}
	}

}

?>
<form action="index.php?id=<?php  echo $page_name; ?><?php if($user_id>0){echo "&newsid=",$user_id;}?>" method="post" class="container"  ENCTYPE="multipart/form-data">
<?php 
if ($user_id)
{
?>
    <input type="hidden" name="user_id" value="<?php  echo $user_id; ?>" />
<?php 
}
?>


<div class="row">

	<fieldset class="col">
	<h5>Informace o uživateli</h5>
	
	
	
	<div class="input-group mb-3">
    <div class="input-group-prepend">
    <span class="input-group-text" id="inputGroup-sizing-default">Jméno</span>
    </div>
    <input type="text"  class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default" value="<?php  echo $row["UName"]; ?>" name="user_name" />
    </div>

	<div class="input-group mb-3">
    <div class="input-group-prepend">
    <span class="input-group-text" id="inputGroup-sizing-default">Příjmení</span>
    </div>
    <input type="text"  class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default" value="<?php  echo $row["USurname"]; ?>" name="user_surname" />
    </div>

	<div class="input-group mb-3">
    <div class="input-group-prepend">
    <span class="input-group-text" id="inputGroup-sizing-default">E-mail</span>
    </div>
    <input type="email" required class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default" value="<?php  echo $row["UEmail"]; ?>" name="UEmail" />
	</div>
	
	<div class="input-group mb-3">
    <div class="input-group-prepend">
    <span class="input-group-text" id="inputGroup-sizing-default">Přihlašovací jméno</span>
    </div>
    <input type="text" required class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default" value="<?php  echo $row["ULogin"]; ?>" name="user_login"  />
    </div>	

	<div class="input-group mb-3">
    <div class="input-group-prepend">
    <span class="input-group-text" id="inputGroup-sizing-default">Heslo</span>
    </div>
    <input type="password" <?php  if (!$user_id) { echo "required";} ?>  class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default" value="" name="user_pass"  />
	</div>	
	
	<div class="input-group mb-3">
    <div class="input-group-prepend">
    <span class="input-group-text" id="inputGroup-sizing-default">Heslo (opakovat)</span>
    </div>
    <input type="password"  <?php  if (!$user_id) { echo "required";} ?>  class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default" value="" name="user_pass_repeat"   />
    </div>	

 
 <div class="input-group mb-3">
  <div class="input-group-prepend">
    <span class="input-group-text">Nahrát foto</span>
  </div>
  <div class="custom-file">
    <input name="userFoto" type="file" class="custom-file-input" id="inputGroupFile01">
    <label class="custom-file-label" for="inputGroupFile01">Vybrat</label>
  </div>
</div>
	
    </fieldset>
	<fieldset class="col">
	
	
	<h5>Oprávnění</h5>




<?php 
$priviledge_query = "Select ID, Description from tblpriviledge where Deleted = 0 order by Description";
$priviledge_res = @$link->query($priviledge_query);
if ($priviledge_res && @mysqli_num_rows($priviledge_res)>0)
{
	$count = 0;
	while ($priviledge_resarr = @mysqli_fetch_array($priviledge_res))
	{

	?>


<div class="custom-control custom-checkbox my-1 mr-sm-2">
    <input <?php  if (is_array($priviledge_arr) && in_array($priviledge_resarr["ID"],$priviledge_arr)) { echo "checked"; } ?> value="<?php  echo $priviledge_resarr["ID"]; ?>" name="privs[]" type="checkbox" class="custom-control-input" id="customControlInline<?php echo $count; ?>">
    <label class="custom-control-label" for="customControlInline<?php echo $count; ?>"><?php  echo $priviledge_resarr["Description"]; ?></label>
  </div>

 
	
	
	
	<?php 
	$count++;
	}
}
?>



<?php 
$query = "Select tblusertype.* from tblusertype where Valid = 1 ORDER BY ID";
$res2 = @$link->query($query);

if ($res2 && @mysqli_num_rows($res2)>0)
{
?>
<select name="user_level" id="user_level"  style="float:left;width: 200px" >
<?php 
while ($row2 = @mysqli_fetch_array($res2))
{
?>
        <option value="<?php  echo $row2['ID']; ?>"<?php  if ($row2["ID"] == intval($row["UType"])) { echo " selected"; } ?>><?php  echo $row2['Description']; ?></option>
<?php 
}
?>
</select>
<?php 
}
?>

 
</fieldset>
</div>


 
<label>Profil</label>
<textarea name="UProfil" style="width:565px; height:150px">
<?php  echo @$row['UProfil']; ?>
</textarea><br />
 

 <div class="custom-control custom-checkbox my-1 mr-sm-2">
 
	<input type="checkbox" name="can_login" class="custom-control-input" id="customControlInline" <?php  if ($row["UCanLogin"] == 1) { echo "checked";} ?>   />
    <label class="custom-control-label" for="customControlInline">Smí se přihlásit</label>
  </div>
 <br/>

  <div class='sender'>       
  <input type="submit" class="btn btn-primary " value="Uložit uživatele" name="send"> 
  </div>
 
</form>

 