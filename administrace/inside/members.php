<?php
$table_name = "tblTeam";
$page_name = "members";

if (isset($_POST['pg']))
{
  $pg =  $_POST['pg'];
  $pglink = '?pg='.$pg;
}
elseif (isset($_GET['pg']))
{
	$pg =  $_GET['pg'];
  $pglink = '?pg='.$pg;		
}
else
{
	$pg = 1;
}
 
$full_path =  Globals::$GLOBAL_WEB_IMGS_PATH;
$fullpathico =  Globals::$GLOBAL_WEB_IMGS_PATH."thumbs/";
 

    //main fotecka
    if (@$_FILES["trener_foto"]["name"] != "")
    {   
        if (@is_uploaded_file($_FILES["trener_foto"]['tmp_name']))
        {
            $path_parts = @pathinfo($_FILES["trener_foto"]["name"],PATHINFO_EXTENSION);

            if ($path_parts == "jpeg" || $path_parts == "jpg" || $path_parts == "JPEG" || $path_parts == "JPG")
            {
                $image_filename = strtotime("now").".".$path_parts;

                $moved = move_uploaded_file($_FILES["trener_foto"]['tmp_name'],$full_path.$image_filename);
            
            
               
                $image_dims =  getimagesize($full_path.$image_filename);
               $image_width = $image_dims[0];
               $image_height = $image_dims[1];
                if ($image_width < 3000 && $image_width > 100 && $image_height < 3000 && $image_height > 100)
                {

                    $aspect_ratio = $image_height / $image_width;
                    $new_image_width = 240;
                    $new_image_height = 240*$aspect_ratio;
                    $new_image_width = intval($new_image_width);
                    $new_image_height = intval($new_image_height);
                    $img = imagecreatefromjpeg($full_path.$image_filename);

                    // color thumb
                    $thumb = @imagecreatetruecolor(240,$new_image_height);
                    @imagecopyresampled($thumb,$img,$width_offset,$height_offset,0,0,$new_image_width,$new_image_height,$image_width,$image_height);
                    imagejpeg($thumb,$full_path."thumbs/".$image_filename);
                    // end of

                    $aspect_ratio = $image_height / $image_width;
                    $new_image_width = 800;
                    $new_image_height = 800*$aspect_ratio;
                    $new_image_width = intval($new_image_width);
                    $new_image_height = intval($new_image_height);
                    $img = imagecreatefromjpeg($full_path.$image_filename);

                    // color thumb
                    $thumb = @imagecreatetruecolor(800,$new_image_height);
                    @imagecopyresampled($thumb,$img,$width_offset,$height_offset,0,0,$new_image_width,$new_image_height,$image_width,$image_height);
                    imagejpeg($thumb,$full_path."box/".$image_filename);
                    // end of

 
                    if($notfirst == 1) { $br = "<br/>"; } else {  $br = "";  }
                    $info_text .= $br."Hlavní obrázek byl uložen a náhled vygenerován.";
                    $notfirst = 1;
                }
                else
                {
 
                    if($notfirst == 1) { $br = "<br/>"; } else {  $br = "";  }
                    $info_text .= $br."Hlavní obrázek je příliš velký nebo příliš malý.";
                    $miniwarning = 1;
                    $notfirst = 1;
                }
            }
            else
            {
 
                if($notfirst == 1) { $br = "<br/>"; } else {  $br = "";  }
                $info_text .= $br."Nepodporovaný formát hlavního obrázku.";
                $miniwarning = 1;
                $notfirst = 1;
            }
        }
    }
 
?>

<h1><?php echo Globals::$GLOBAL_MODULES_MEMBERS; ?> - přehled </h1>

<form action="index.php?id=<?php echo $page_name; ?>" method="post">

<?php
  
if(isset($_REQUEST["delete"]) && is_numeric($_REQUEST["delete"]) && $_REQUEST["delete"] > 0)
{
    $delete = $_REQUEST["delete"];
    $query = "DELETE FROM ".$table_name."  WHERE id = ".$_REQUEST["delete"];
    $del_res = $link->query($query);
    if ($del_res)
    {
        if (mysql_affected_rows($link) > 0)
        {

            if($notfirst == 1) { $br = "<br/>"; } else {  $br = "";  }
            $info_text .= $br."Položka byl smazána.";
 
            $notfirst = 1;
        }
        else
        {
 
            if($notfirst == 1) { $br = "<br/>"; } else {  $br = "";  }
            $info_text .= $br."Položka nebylo smazána.";
            $miniwarning = 1;
            $notfirst = 1;
        }
    }
    else
    {
 
        if($notfirst == 1) { $br = "<br/>"; } else {  $br = "";  }
        $info_text .= $br."Chyba během mazání položky.";
        $miniwarning = 1;
        $notfirst = 1;
    }
}

if (isset($_REQUEST["activation"]))
{
	$activ_id = $_REQUEST["activation"];
  if($_REQUEST["public"] == 1){
	$activquery = "UPDATE ".$table_name." SET logoPublic=1 Where ID =".$activ_id;
	}
	elseif($_REQUEST["public"] == 0){
	$activquery = "UPDATE ".$table_name." SET logoPublic=0 Where ID =".$activ_id;
	}
	$activres = $link->query($activquery);
	if ($activres)
	{
		if (mysqli_affected_rows($link) > 0)
		{
 
            if($notfirst == 1) { $br = "<br/>"; } else {  $br = "";  }
            $info_text .= $br." Změněn stav publikace.";
            $notfirst = 1;
		}
	}
}
 

if (isset($_REQUEST["high"]))
{
	$activ_id = $_REQUEST["newsid"];
  if($_REQUEST["high"] == 1){
	$activquery = "UPDATE ".$table_name." SET public=1 Where ID =".$activ_id;
	}
	elseif($_REQUEST["high"] == 0){
	$activquery = "UPDATE ".$table_name." SET public=0 Where ID =".$activ_id;
	}
	$activres =  $link->query($activquery);
	if ($activres)
	{
		if (@mysqli_affected_rows($link) > 0)
		{
            if($notfirst == 1) { $br = "<br/>"; } else {  $br = "";  }
            $info_text .= $br."Změněn stav publikace.";
            $notfirst = 1;

		}
	}
}


    if (isset($_REQUEST["send"]) && isset($_REQUEST["newsid"]) && is_numeric($_REQUEST["newsid"]) && $_REQUEST["newsid"]>0)
    {
        if (strlen(trim($_POST["trener_name"])))
        {
        
            $query = "Update ".$table_name." set ";
        
            $col_query =" trener_name";
            $val_query ="  '".trim(@$_POST["trener_name"])."'";
            $query .= $col_query." = ".$val_query;
            
            $col_query =" , trener_text";
            $val_query ="  '".trim(@$_POST["trener_text"])."'";
            $query .= $col_query." = ".$val_query;  
            
            if($image_filename){
            $col_query =" , trener_foto";
            $val_query ="  '".$image_filename."'";
            $query .= $col_query." = ".$val_query;                             
            }
            if (isset($_POST["public"]))
            {
                $col_query =", public";
                $val_query ="  1";
                $query .= $col_query." = ".$val_query;
            }
            else
            {
                $col_query =", public";
                $val_query ="  0";
                $query .= $col_query." = ".$val_query;
            }   
            
            $query .= " where ID = ".$_REQUEST["newsid"];            
             // echo $query; 
            $res = $link->query($query);

            if ($res && mysqli_affected_rows($link)>0)
            {
 
                if($notfirst == 1) { $br = "<br/>"; } else {  $br = "";  }
                $info_text .= $br."Položka byla úspěšně uložena.";
                $notfirst = 1;
            }
            else
            {
 
                if($notfirst == 1) { $br = "<br/>"; } else {  $br = "";  }
                $info_text .= $br."Položka nebyla změněna.";
                $notfirst = 1;
                $miniwarning = 1;
            }
        }
        else
        {
 
            if($notfirst == 1) { $br = "<br/>"; } else {  $br = "";  }
            $info_text .= $br."Chyba během ukládání položky. Nebyly zadány všechny povinné parametry";
            $notfirst = 1;
            $miniwarning = 1;
        }
    }
//posila novou
else{

        if (@$_REQUEST["send"]) 
      
        {
        
        if(1==1){

            $query = "Insert into ".$table_name;

            $col_query = "ID";
            $val_query = "''";
        
            $col_query .=", trener_name";
            $val_query .="   , '".trim(@$_POST["trener_name"])."'";
        
        if($image_filename){
            $col_query .=", trener_foto";
            $val_query .="   , '".trim($_POST["trener_foto"])."'";

            }           

            $col_query .=", trener_text";
            $val_query .="   , '".trim(@$_POST["trener_text"])."'";
 
            if (isset($_POST["public"]))
            {
                $col_query .=", public";
                $val_query .="  , 1";
            }
            else
            {
                $col_query .=", public";
                $val_query .="  , 0";
            }                     
              
             $query .= "(".$col_query.") values (".$val_query.")"; 
            $res = $link->query($query);            
            if ($res && mysqli_affected_rows($link)>0)
            {
                if($notfirst == 1) { $br = "<br/>"; } else {  $br = "";  }
                $info_text .= $br."Položka úspěšně uložena.";
                $notfirst = 1;
  
            }
            else
            {
                if($notfirst == 1) { $br = "<br/>"; } else {  $br = "";  }
                $info_text .= $br."Položka nebyla uložena.";
                $notfirst = 1;
                $miniwarning = 1;
            }
        }
        else
        {
            if($notfirst == 1) { $br = "<br/>"; } else {  $br = "";  }
            $info_text .= $br."Chyba během ukládání položky Nebyly zadány všechny povinné parametry!";
            $notfirst = 1;
            $miniwarning = 1;
        }
}

}


if ($info_text!=""){
    if($miniwarning == 0) {  $miniwarningclass = 'alert-info' ; } 
    elseif($miniwarning == 1) {  $miniwarningclass = 'alert-danger' ; } 
    else {  $miniwarningclass = 'alert-success' ; }
?>

<div class="alert <?php echo $miniwarningclass; ?> role="alert"><?php echo $info_text; ?></div>

<?php
}

$sc = 0;

$th[$sc] = "Public";
$tc[$sc] = "short center";
$td[$sc] = "public";
$sc++;

$th[$sc] = "Smazat";
$tc[$sc] = "short center";
$td[$sc] = "delete";
$sc++;

$th[$sc] = "Trenér";
$tc[$sc] = "";
$td[$sc] = "trener_name";
 
$time=time();
 
 
 $totquery = "SELECT count(*) AS Pocet FROM $table_name   ".$where;
$totresult=$link->query($totquery);
  
$kolik = Globals::$GLOBAL_ADMIN_PAGER ;


    if ($totresult && mysqli_num_rows($totresult) == 1) 
    { 
        $totalrow =  mysqli_fetch_array($totresult); 
         $rows  = $totalrow["Pocet"]; 
    }
    else  
    { 
        $rows  = 0; 
    }  
  
?>
<table   class="table table-striped table-bordered m-b-0 toggle-circle footable-loaded footable tablet breakpoint"> 
<tr>
 <?php
  for ($tci = 0; $tci <= $sc; $tci++) {
   ?>
   <th class='<?php echo $tc[$tci]; ?>'><?php echo $th[$tci]; ?></th>
   <?php
    }

echo "</tr>";

$pager = new Pager(); $pager->results_per_page = $kolik; 

$orderby = " ID Desc ";

 $query = "SELECT *   FROM ".$table_name."   Where 1=1 ".$where." ".$group."  Order by  ".$orderby.pagerNewStart($kolik,$pg,$rows) ;

$result = $link->query($query); 
 
if ($result && mysqli_num_rows($result)>0)
{
while ($row2 = mysqli_fetch_array($result)){
    
/* 
if($type == 1) { $fileColor = "#FFFFCC"; }elseif($type == 2){$fileColor = "#cdfe92";}elseif($type == 3){$fileColor = "#DEFBFF";}else{ $fileColor = "#FFFFCC"; }
$now = time();
*/

for ($tci = 0; $tci <= $sc; $tci++) {
    
    ?>
      <td class='<?php echo $tc[$tci]; ?>'>
      <?php 
 
      echo showInTable( $td[$tci] , $row2[$td[$tci]] , $maxporadi , $minporadi , $menucolor , $row2[ID] , $page_name , $menu ,$tf[0] );
      ?>
      </td>
    <?php
    }
    ?>
  </tr>
 
<?php 
}
}

?>     
</table>   

<?php
          if (isset($_GET['pg']))
          {
              $hide = "&pg=".$_GET['pg'];
              $prepend_path = str_replace ($hide , '' , $_SERVER['REQUEST_URI'] );
          }
          else{
            $prepend_path = $_SERVER['REQUEST_URI'];
          }
?>

 <nav aria-label="...">
 <ul class="pagination">
<?php
     
 

    echo $pages = $pager->getPages($rows, $pg, $prepend_path);
?>

  </ul>
</nav>
 
    <a href="index.php?id=<?php echo $page_name; ?>new" class="fas fa-plus-circle"><span >Přidat další</span></a> 
