

<style>
.input-group-prepend, .input-group-text {
    min-width: 50px;
}
</style>
  <fieldset class="col-lg-12  col-sm-12" id="otazka1"> 

<legend>Otázka č.1</legend>

<div class="input-group mb-3">
    <div class="input-group-prepend">
        <span class="input-group-text" id="inputGroup-sizing-default">
        Text otázky:
    </span>
</div>
<input name="questName[1]" id="questName[1]" type="text" value="<? echo $row["questName"]; ?>" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default">
</div>

 
<div class="input-group mb-3">
  <div class="input-group-prepend">
  <label class="input-group-text" for="inputGroupSelect01">Typ otázky:</label>
  </div>
  <div class="form-control one-input">
  <select name="questionTyp[1]" id="sel1" onChange="checkSelect(sel1)">
    <option value="1">Odpovědět textem</option>
    <!--<option value="2">Označení odpovědi na škále</option>-->
    <option value="3">Zaškrtávání možností - pouze jednu</option>
    <option value="4">Zaškrtávání možností - více možností</option>
 </select>
</div>
</div>
<input type="hidden" name="questionOrder[1]" value="1" />

 

<div id="semka1"> </div>
<!--<a style="float:right" class="nextqest" onClick="nextQuest(1)" title="Další otázka"></a>-->

</fieldset>
<div id="tu1" class='qceep col-lg-12  col-sm-12 first'></div>
 