
<?php
function dekode($text){ 
$co = array ('ž',     'Ž',     'š',     'Š',     'č',     'Č',     'ř',      'Ř',    'ď',     'Ď',     'ť',     'Ť',      'ň',     'Ň',     'á',     'Á',     'ě',     'Ě',      'é',    'É',     'í',     'Í',    'ó',      'Ó',     'ú',      'Ú',    'ů',     'Ů',     'ý',     'Ý'); 
$cim = array('&#158;','&#142;','&#154;','&#138;','&#269;','&#268;','&#345;','&#216;','&#239;','&#207;','&#357;','&#356;', '&#328;','&#327;','&#225;','&#193;','&#283;','&#282;','&#233;','&#201;','&#237;','&#205;','&#243;','&#211;','&#250;','&#218;','&#367;','&#366;','&#253;','&#221;'); 
//return str_replace($co, $cim, $text); 
 return $text;
} 

$number = $_REQUEST['number'];
 
?>

<div class="input-group mb-3">
    <div class="input-group-prepend">
        <span class="input-group-text" id="inputGroup-sizing-default">
        Nejméně:
    </span>
</div>
<input required ='required' name="question2Answer1[<?php echo $number; ?>]" id="question2Answer1[<?php echo $number; ?>]" type="text"  class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default" data-toggle="tooltip" data-placement="top" title="Pole Nejméně u Otázky č.<?php echo $number; ?> je povinné"  />
</div>

<div class="input-group mb-3">
    <div class="input-group-prepend">
        <span class="input-group-text" id="inputGroup-sizing-default">
        Nejvíce:
    </span>
</div>
<input required ='required' name="question2Answer2[<?php echo $number; ?>]" id="question2Answer2[<?php echo $number; ?>]" type="text"   class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default" data-toggle="tooltip" data-placement="top" title="Pole Nejvíce u Otázky č.<?php echo $number; ?> je povinné"  />
</div>

<div class="input-group mb-3">
  <div class="input-group-prepend">
  <label class="input-group-text" for="inputGroupSelect01">Rozpětí škály:</label>
  </div>
  <div class="form-control one-input">
  <select name="questionScaleCount[<?php echo $number; ?>]">
    <option value="3">3</option>
    <option value="4">4</option>
    <option value="5">5</option>
    <option value="6">6</option>
    <option value="7">7</option>
    <option value="8">8</option>
    <option value="9">9</option>
    <option value="10">10</option>
 </select>
</div>
</div>
 
