<h1>DEtail banneru</h1>
<?php
 $lang = $_GET['lang'];
//include ("./lib/lib.php");
if($newsid){
 
$result= $link->query("SELECT * FROM tblbanners WHERE bannerID=".$newsid);

$public = mysqli_result($result,0,"bannerPublic");
$bannerHeight = mysqli_result($result,0,"bannerHeight");

$flash =mysqli_result($result,0,"bannerFlash");
$noflash =mysqli_result($result,0,"bannerJPG");
$noflashgif =mysqli_result($result,0,"bannerGif");

$bannerForm =mysqli_result($result,0,"bannerFrom");
$bannerTo =mysqli_result($result,0,"bannerTo");
$extraHP =mysqli_result($result,0,"extraHP");
$bannerCamp =mysqli_result($result,0,"bannerCamp");
$bannerViews = mysqli_result($result,0,"bannerViews");


if($_GET['lang'] == 'en') {
    $name=mysqli_result($result,0,"bannerNameEN");
    $bannerVars = mysqli_result($result,0,"bannerVarsEN");
    $bannerUrl=mysqli_result($result,0,"bannerUrlEN");
    $pageres = $link->query("SELECT ID, pagesName, pagesLink  From tblpages_en");  
}
elseif($_GET['lang'] == 'de') {
    $name=mysqli_result($result,0,"bannerNameDE");
    $bannerVars = mysqli_result($result,0,"bannerVarsDE");
    $bannerUrl=mysqli_result($result,0,"bannerUrlDE");
    $pageres = $link->query("SELECT ID, pagesName, pagesLink  From tblpages_de");  
}
elseif($_GET['lang'] == 'pl') {
    $name=mysqli_result($result,0,"bannerNamePL");
    $bannerVars = mysqli_result($result,0,"bannerVarsPL");
    $bannerUrl=mysqli_result($result,0,"bannerUrlPL");
    $pageres = $link->query("SELECT ID, pagesName, pagesLink  From tblpages_pl");  
}            
else{
    $name=mysqli_result($result,0,"bannerName");
    $subname=mysqli_result($result,0,"bannerSubName");
    $bannerVars = mysqli_result($result,0,"bannerVars");
    $bannerUrl=mysqli_result($result,0,"bannerUrl");
    $pageres = $link->query("SELECT ID, pagesName, pagesLink  From tblpages");  
}



}
else{
$flash ='ne';
$noflash ='ne';
$noflashgif ='ne';
}

 


function thumbNailsBanner($picturename){
    $file = "../Uploads/Banners/".$picturename;
    if(File_Exists ($file)){
    $show = "../Uploads/Banners/".$picturename;
    }
    else{
    $show = "../Uploads/Banners/none.jpg";
    }
    return $show;
 }

?>


<form  action="index.php?id=banners<?php if($newsid>0){echo "&newsid=",$newsid;}?><?php if($lang){echo "&lang=".$lang; } ?>" method="post" name="noname" ENCTYPE="multipart/form-data">

<div class="row">

<fieldset class="col">


 <div class="input-group mb-3">
    <div class="input-group-prepend">
    <span class="input-group-text" id="inputGroup-sizing-default">Hlavní text</span>
    </div>
    <input type="text" required class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default" value="<?php if($newsid){ echo $name; } ?>" name="bannerName" />
    </div>


    <div class="input-group mb-3">
    <div class="input-group-prepend">
    <span class="input-group-text" id="inputGroup-sizing-default">Text</span>
    </div>
    <input type="text"   class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default" value="<?php if($newsid){ echo $subname; } ?>" name="bannerSubName" />
    </div>    

	<div class="input-group mb-3">
    <div class="input-group-prepend">
    <span class="input-group-text" id="inputGroup-sizing-default">Odkaz</span>
    </div>
    <input type="text"   class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default" value="<?php if($newsid){ echo $bannerUrl; }else{echo "http://";} ?>" name="bannerUrl" />
    </div>  


<h5>Umístění</h5>
 <?php
 if ($pageres && mysqli_num_rows($pageres)>0)
 {
   while ($prow = @mysqli_fetch_array($pageres)){   
   $checkeds = explode(',',$bannerVars);
   ?> 
<div class="custom-control custom-checkbox my-1 mr-sm-2">
    <input <?php if (in_array($prow['pagesLink'], $checkeds))  { echo " checked "; }   ?>  value="<?php echo $prow['pagesLink'] ?>" id="my<?php echo $prow['ID'] ?>" name="bannerVars[]" type="checkbox" class="custom-control-input"  >
    <label class="custom-control-label" for="my<?php echo $prow['ID'] ?>"><?php echo $prow['pagesName'] ?></label>
  </div>   
<?php
    }
    ?>
<div class="custom-control custom-checkbox my-1 mr-sm-2">
    <input <?php if (in_array('home', $checkeds))  { echo " checked "; }   ?>  value="home" id="my<?php echo $prow['ID'] ?>" name="bannerVars[]" type="checkbox" class="custom-control-input"  >
    <label class="custom-control-label" for="my<?php echo $prow['ID'] ?>">Homepage</label>
  </div>  
    <?php
}
?>

 <div class="input-group mb-3">
    <div class="input-group-prepend">
    <span class="input-group-text" id="inputGroup-sizing-default">Posun banneru</span>
    </div>
    <select name='bannerViews'>
    <option value="top" <?php if($bannerViews == 'top') { echo ' selected '; } ?> >top</option>
    <option value="10%" <?php if($bannerViews == '10%') { echo ' selected '; } ?> >10%</option>
    <option value="20%" <?php if($bannerViews == '20%') { echo ' selected '; } ?> >20%</option>
    <option value="30%" <?php if($bannerViews == '30%') { echo ' selected '; } ?> >30%</option>
    <option value="40%" <?php if($bannerViews == '40%') { echo ' selected '; } ?> >40%</option>
    <option value="center" <?php if($bannerViews == 'center') { echo ' selected '; } ?> >center</option>
    <option value="60%" <?php if($bannerViews == '60%') { echo ' selected '; } ?> >60%</option>
    <option value="70%" <?php if($bannerViews == '70%') { echo ' selected '; } ?> >70%</option>    
    <option value="80%" <?php if($bannerViews == '80%') { echo ' selected '; } ?> >80%</option>
    <option value="90%" <?php if($bannerViews == '90%') { echo ' selected '; } ?> >90%</option>
    <option value="bottom" <?php if($bannerViews == 'bottom') { echo ' selected '; } ?> >bottom</option>
</select>
 
    </div>


 <div class="input-group mb-3">
  <div class="input-group-prepend">
    <span class="input-group-text">Nahrát nový banner JPG/PNG/MP4</span>
  </div>
  <div class="custom-file">
    <input name="file3" type="file" class="custom-file-input" id="inputGroupFile01"  />
    <label class="custom-file-label" for="inputGroupFile01">Vybrat</label>
  </div>
</div>


<?php if($newsid){
$result = $link->query("SELECT bannerFrom FROM tblbanners WHERE bannerID=$newsid");
$kdy = mysqli_result($result,0,"bannerFrom");
$denn = date("d",$kdy);
$mes = date("m",$kdy);
$rokk = date("Y",$kdy);
$kdyformat = $rokk."-".$mes."-".$denn;
 
}else{
$kdy = time();
$denn = date("d",$kdy);
$mes = date("m",$kdy);
$rokk = date("Y",$kdy);
$kdyformat = $rokk."-".$mes."-".$denn;
}
?>

<?php if($newsid){
$result=$link->query("SELECT bannerTo FROM tblbanners WHERE bannerID=$newsid");
$kdy = mysqli_result($result,0,"bannerTo");
$denn = date("d",$kdy);
$mes = date("m",$kdy);
$rokk = date("Y",$kdy);
$kdyformat2 = $rokk."-".$mes."-".$denn;
}else{
$kdy = time()+ 2592000;
$denn = date("d",$kdy);
$mes = date("m",$kdy);
$rokk = date("Y",$kdy);
$kdyformat2 = $rokk."-".$mes."-".$denn;
}
?>

 
<!--
  <div class="input-group mb-3"
    <div class="input-group-prepend">
    <span class="input-group-text" id="inputGroup-sizing-default">Start / Konec</span>
    </div>
    <input  type="date" required class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default"    value="<?php echo $kdyformat; ?>"  name="kdy" id="kdy" />
    <input  type="date" required class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default"    value="<?php echo $kdyformat2; ?>"  name="kdy2" id="kdy2" />
	</div>
-->

</fieldset>

<fieldset class="col">

<h5>Aktuální banner</h5>
<div class="card-photo"  style="height: auto; clear: left;">
      <?php if( strpos($noflash,'mp4') > 0 ) {  echo "Video"; }  else {  ?>
       <img style='width: 100%; height: auto'src="<?php $picturename=$noflash; echo thumbNailsBanner($picturename); ?>" title="<?php $picturename=$noflash; echo thumbNailsBanner($picturename); ?> | <?php echo $picturename; ?>"/>
      <?php } ?>
 
</div>

<div class="custom-control custom-checkbox my-1 mr-sm-2">
    <input <?php if($newsid){ if ($public=='1'){echo " checked";}}else{ echo " checked "; } ?>  value="<?php echo $prow['pagesLink'] ?>" id="my<?php echo $prow['ID'] ?>" name="bannerPublic" type="checkbox" class="custom-control-input"  >
    <label class="custom-control-label" for="my<?php echo $prow['ID'] ?>">Povolit zobrazování</label>
  </div>  
 
 

</fieldset>
</div>

 
 <div class='sender'>       
  <input type="submit" class="btn btn-lg btn-primary " value="Uložit banner" name="send" /> 
  </div>
</form>
