(function( $ ) {
	var methods = {
		init: function () {
			$(".selectAll").each(function () {
				$(this).bind("click", function (event) {
					$.fn.chboxSelectAll("select", {"event" : event});
				});
			});
		},
		select: function (params) {
			var event = $.event.fix(params.event);
			
			var tmpWhat = $(event.target).attr("name").substring(9, $(event.target).attr("name").length);
			
			var setChecked = ($(event.target).data("selected") != true) ? true : false;
			$(event.target).data("selected", setChecked);
			
			$("input[name^="+tmpWhat+"]").prop("checked", setChecked);
		}
	};
	$.fn.chboxSelectAll = function (method) {
		if (methods[method]) {
			return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
		}
		else if (typeof method === "object" || !method) {
			return methods.init.apply(this, arguments);
		}
		else {
			$.error("Method "+method+" does not exist on jQuery.cookie");
		}
	};
})( jQuery );

$(document).ready(function () {
	$.fn.chboxSelectAll("init");
});